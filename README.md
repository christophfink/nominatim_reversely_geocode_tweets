Nominatim API Python engine
===========================

This python script reads in CSV files (`lat;lon;id`), in this case with 540 million rows, carries out reverse geocoding by querying a Nominatim instance (via its HTTP REST API), and saves the output to a new CSV file. It is using `multiprocessing`, and thus can process around 400 rows per second on our 2x Xeon E5-2660, 128 GiB test machine.
There is substantial potential for optimisation, especially in the way this script is linked to the Nominatim engine. Linking a Python program with okay-ish performance to a C program via an HTTP API and a PHP backend is definitely creating significant overhead. As this project's most important factor was a timely development, no attempts have been made to alter the Nominatim project's source.

